<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    // start datatables
    var $table = 'cms_menu_settings';
    var $column_order = array(null, 'menu_name', 'link', 'is_active');
    var $column_search = array('menu_name', 'link'); //set column field database for datatable searchable
    var $order = array('id' => 'desc'); // default order

    private function _get_datatables_query()
    {
        $this->db->select('*');
        $this->db->from('cms_menu_settings');

        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('cms_menu_settings');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function getMenuSettings()
    {
        return $this->db->get('cms_menu_settings');
    }
    public function getSubMenu()
    {
        $query = "SELECT `cms_user_sub_menu`.*, `cms_user_menu`.`menu`
                  FROM `cms_user_sub_menu` JOIN `cms_user_menu`
                  ON `cms_user_sub_menu`.`menu_id` = `cms_user_menu`.`id`";

        return $this->db->query($query)->result_array();
    }

    function edit_menu($id, $menu)
    {
        $hasil = $this->db->query("UPDATE `cms_user_menu` SET `menu`='$menu' WHERE `id`='$id'");
        return $hasil;
    }

    function delete_menu($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function delete_menu_setting($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("cms_menu_settings");
    }
}
