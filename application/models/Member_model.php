<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    // start datatables
    var $table = 'cms_user_student';
    var $column_order = array(null, 'a.name', 'a.email', 'a.phone', 'a.photo', 'b.nama_paket', 'a.status_paket', 'a.status');
    var $column_search = array('a.name', 'a.email', 'a.phone'); //set column field database for datatable searchable
    var $order = array('a.id' => 'desc'); // default order

    private function _get_datatables_query()
    {
        $this->db->select('a.*, b.nama_paket');
        $this->db->from('cms_user_student as a');
        $this->db->join('cms_paket_settings as b', 'b.id = a.paket', 'left');

        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_count()
    {
        $this->db->from($this->table);
        $this->db->where('status_paket', '1');
        return $this->db->count_all_results();
    }

    function update_approve_member($id, $data, $table)
    {
        $this->db->where($id);
        $this->db->update($table, $data);
    }

    public function get_by_id($id)
    {
        $this->db->select('a.*, b.nama_paket');
        $this->db->from('cms_user_student as a');
        $this->db->join('cms_paket_settings as b', 'b.id = a.paket', 'left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function upgrade_paket($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function config_bayar_upgrade_paket()
    {
        $this->db->select('*');
        $this->db->from('cms_settings');
        $this->db->where('id', 12);
        $query = $this->db->get();
        return $query->row();
    }
}
