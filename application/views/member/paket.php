<!--================Paket Area =================-->
<section class="ftco-section" style="padding-top: 150px;">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="section_tittle text-center">
                <p>Our Services</p>
                <h2>Buddy you can rely on</h2>
            </div>
        </div>
        <div class="row tabulation mt-4 ftco-animate">
            <div class="col-md-4">
                <ul class="nav nav-pills nav-fill d-md-flex d-block flex-column">
                    <li class="nav-item text-left">
                        <a class="nav-link active py-4" data-toggle="tab" href="#services-1">CV Clinic and Improvement</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-4" data-toggle="tab" href="#services-2">Interview Preparation</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-4" data-toggle="tab" href="#services-3">Psychotest Drilling</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-4" data-toggle="tab" href="#services-4">Career Consultation</a>
                    </li>
                    <!-- <li class="nav-item text-left">
                        <a class="nav-link py-4" data-toggle="tab" href="#services-5">Personal Problem</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-4" data-toggle="tab" href="#services-6">Business Problem</a>
                    </li> -->
                </ul>
            </div>
            <div class="col-md-8">
                <div class="tab-content">
                    <div class="tab-pane container p-0 active" id="services-1">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_1.jpg'); ?>);"></div>
                        <h3><a href="#">CV Clinic and Improvement</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-2">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_2.jpg'); ?>);"></div>
                        <h3><a href="#">Interview Preparation</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-3">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_3.jpg'); ?>);"></div>
                        <h3><a href="#">Psychotest Drilling</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-4">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_4.jpg'); ?>);"></div>
                        <h3><a href="#">Career Consultation</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <!-- <div class="tab-pane container p-0 fade" id="services-5">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_5.jpg'); ?>);"></div>
                        <h3><a href="#">Personal Problem</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-6">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image-6.jpg'); ?>);"></div>
                        <h3><a href="#">Business Problem</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="feature_part">
    <div class="container">
        <?= $this->session->flashdata('message_upgrade_paket'); ?><br /><br />
        <div class="row justify-content-center">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>Pilih Paket</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" style="padding-bottom: 120px;">
            <?php foreach ($paket_pilih as $pp) : ?>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="<?= $pp['icon']; ?>"></i></span>
                            <h4><?= $pp['nama_paket']; ?></h4>
                            <p><?= $pp['keterangan']; ?></p>
                            <br />
                            <p>Rp. <?= number_format($pp['harga_paket'], 2, ',', '.'); ?></p>
                            <br />
                            <?php foreach ($data_profil as $dp) : ?>
                                <?php if ($pp['id'] == $dp['paket']) {
                                    echo "<button type=\"button\" class=\"btn_2\">Paket Anda</button>";
                                } else if ($dp['paket'] == 0 && $dp['status_paket'] == 0) {
                                    echo "<a href='" . base_url('member/profil/') . $pp['id'] . "' class=\"btn post-btn\">Pilih Paket</a>";
                                } else if ($dp['paket'] < $pp['id']) {
                                    echo "<a href=\"#\" data-toggle=\"modal\" data-target=\"#upgrade\" class=\"btn post-btn\">Upgrade</a>";
                                } else if ($dp['paket'] > $pp['id']) {
                                    echo "<button class=\"btn post-btn\" disabled>Pilih Paket</button>";
                                } ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<div id="upgrade" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin: absolut; top: 15%; padding: 10px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Upgrade Paket</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="<?= base_url('member/upgrade_paket'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">Nama Lengkap :</label>
                        <input type="text" class="form-control" id="full_name" name="full_name" value="<?= $user_data->name; ?>" required readonly="true">
                    </div>
                    <div class="form-group">
                        <label for="usr">Email :</label>
                        <input type="text" class="form-control" id="email" name="email" value="<?= $user_data->email; ?>" required readonly="true">
                    </div>
                    <div class="form-group">
                        <label for="usr">No Handphone Aktif :</label>
                        <input type="text" class="form-control" id="phone" name="phone" value="<?= $user_data->phone; ?>" required readonly="true">
                    </div>
                    <div class="form-group">
                        <label for="usr">Pilihan Paket :</label>
                        <div class="form-group">
                            <!-- PHP CODING BUAT AMBIL LIST PAKET -->
                            <select id="paket" name="paket" class="form-control" required>
                                <option value="">- Pilih Paket -</option>
                                <?php foreach ($paket_pilih as $pp) : ?>
                                    <option value="<?= $pp['id']; ?>" <?php if ($pp['id'] <= $user_data->paket) {
                                                                            echo "hidden";
                                                                        } ?>><?= $pp['nama_paket']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <!-- PHP CODING BUAT AMBIL LIST PAKET -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <h6 style="position: absolute; left:0px; padding-left:15px;">Bingung ? <a href="whatsapp:contact=081806122995@s.whatsapp.com&message=' I&#39;d like to chat with you\'" style='color: #f44a40;'>Hubungi Admin</a></h6>
                    <button id=" beli" type="submit" class="btn post-btn">OK, Saya Upgrade</button>
                </div>
            </form>
        </div>
    </div>
</div>