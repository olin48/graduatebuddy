<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
        <!-- <div class="col-lg-12">
            <div class="box">
                <div class="box-body">
                    <div class="news-widget">
                        <h2>
                            <span class="bg-red">Keterangan Paket</span>
                        </h2>
                    </div>
                    <p style="font-size:15px;"><b>Custom HTML</b></p>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_kiri" title="Edit"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
            </div>
        </div> -->
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data Testimonial</span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_testimonial">Tambah Paket</button> -->
                    <br /><br />
                    <table id="dataTestimonial" class="table table-bordered table-striped" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 16%;">Nama Member</th>
                                <th>Profesi</th>
                                <th>Komentar</th>
                                <th>Photo</th>
                                <th>Status</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <!-- Modal -->
                <div id="add_testimonial" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Tambah Testimonial :</h4>
                            </div>
                            <?php echo form_open_multipart('cms/add_testimoni'); ?>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="usr">Nama Member :</label>
                                    <input type="text" class="form-control" id="nama_member_add" name="nama_member_add" required>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Profesi :</label>
                                    <input type="text" class="form-control" id="profesi_add" name="profesi_add" required>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Komentar :</label>
                                    <textarea class="form-control" id="komentar_add" name="komentar_add" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Photo :</label>
                                    <input type="file" class="form-control-file" id="logo" name="logo"></input>
                                    <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status :</label>
                                    <select name="status_id" id="status_id" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="0">Non Active</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

                <div id="edit_promosi" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Paket</h4>
                            </div>
                            <?php echo form_open_multipart('cms/edit_testimoni'); ?>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="usr">Nama Member :</label>
                                    <input type="hidden" class="form-control" id="testimoni_id_edit" name="testimoni_id_edit" required>
                                    <input type="text" class="form-control" id="nama_member_edit" name="nama_member_edit" required>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Profesi :</label>
                                    <input type="text" class="form-control" id="profesi_edit" name="profesi_edit" required>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Komentar :</label>
                                    <textarea class="form-control" id="komentar_edit" name="komentar_edit" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Photo :</label>
                                    <input type="hidden" id="old_logo" name="old_logo" />
                                    <input type="file" class="form-control-file" id="logo" name="logo"></input>
                                    <i>Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i></br>
                                    <span id="image_edit"></span>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Status :</label>
                                    <select name="status_id" id="status_id" class="form-control" required>
                                        <option value="">Pilih Status</option>
                                        <option value="0">Non Active</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div id="edit_kiri" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Custom HTML</h4>
                            </div>

                            <?php echo form_open_multipart('cms/update_paket_kiri'); ?>
                            <div class="modal-body">
                                <?php foreach ($kiri as $kir) : ?>
                                    <div class="form-group">
                                        <label for="usr">Title :</label>
                                        <input type="text" class="form-control" id="title-kiri" name="title-kiri" value="<?= $kir['judul']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Content :</label>
                                        <textarea class="form-control" id="content-kiri" name="content-kiri" style="height: 250px"><?= $kir['content']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Link Read More :</label>
                                        <input type="text" class="form-control" id="link-kiri" name="link-kiri" value="<?= $kir['link']; ?>">
                                    </div>

                                <?php endforeach; ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

            </div>
            <!-- #/paper bg -->
        </div>
        <!-- ./wrap-sidebar-content -->

        <!-- / END OF CONTENT -->