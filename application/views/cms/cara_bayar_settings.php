<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <?= $this->session->flashdata('message'); ?>
        <div class="col-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <p style="font-size:15px;"><b>Cara Bayar Settings</b> - Cara Pembayaran Paket</p>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_bawah"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div id="edit_bawah" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cara Bayar</h4>
                </div>

                <?php echo form_open_multipart('cms/update_cara_bayar'); ?>
                <div class="modal-body">
                    <?php foreach ($cara_bayar as $cb) : ?>
                        <div class="form-group">
                            <label for="usr">Title :</label>
                            <input type="text" class="form-control" id="title-bayar" name="title-bayar" value="<?= $cb['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Content :</label>
                            <textarea class="form-control" id="content-bayar" name="content-bayar" style="height: 250px"><?= $cb['content']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="usr">Link Konfirmasi Pembayaran :</label>
                            <input type="text" class="form-control" id="link-bayar" name="link-bayar"><?= $cb['link']; ?></input>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->