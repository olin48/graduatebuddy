<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_paket">Tambah Paket</button>
                    <br /><br />
                    <table id="dataPaket" class="table table-bordered table-striped" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th style="width: 16%;">Nama Paket</th>
                                <th>Keterangan</th>
                                <th>Harga Paket</th>
                                <th>Icon</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <!-- Modal -->
                <div id="add_paket" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Post Paket</h4>
                            </div>
                            <?php echo form_open_multipart('cms/add_paket'); ?>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="usr">Nama Paket :</label>
                                    <input type="text" class="form-control" id="paket_name_add" name="paket_name_add" required>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Keterangan :</label>
                                    <textarea class="form-control" id="description_add" name="description_add" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Harga Paket :</label>
                                    <input type="text" class="form-control" id="harga_paket_add" name="harga_paket_add" required>
                                </div>
                                <div class="form-group">
                                    <label for="usr">Icon :</label>
                                    <input type="text" class="form-control" id="icon_add" name="icon_add" required>
                                    <a href="https://themify.me/themify-icons" target="_blank">
                                        <font style="font-size: 10px;"><b>Refrensi Icon</b></font>
                                    </a>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

                <div id="edit_paket" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Paket</h4>
                            </div>
                            <form action="<?php echo base_url('cms/edit_paket'); ?>" id="form_edit_paket" method="post">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="usr">Nama Paket :</label>
                                        <input type="hidden" class="form-control" id="paket_id_edit" name="paket_id_edit" required>
                                        <input type="text" class="form-control" id="paket_name_edit" name="paket_name_edit" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Keterangan :</label>
                                        <textarea class="form-control" id="description_edit" name="description_edit" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Harga Paket :</label>
                                        <input type="text" class="form-control" id="harga_paket_edit" name="harga_paket_edit" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Icon :</label>
                                        <input type="text" class="form-control" id="icon_edit" name="icon_edit" required>
                                        <a href="https://themify.me/themify-icons" target="_blank">
                                            <font style="font-size: 10px;"><b>Refrensi Icon</b></font>
                                        </a>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div id="edit_kiri" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Custom HTML</h4>
                            </div>

                            <?php echo form_open_multipart('cms/update_paket_kiri'); ?>
                            <div class="modal-body">
                                <?php foreach ($kiri as $kir) : ?>
                                    <div class="form-group">
                                        <label for="usr">Title :</label>
                                        <input type="text" class="form-control" id="title-kiri" name="title-kiri" value="<?= $kir['judul']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Content :</label>
                                        <textarea class="form-control" id="content-kiri" name="content-kiri" style="height: 250px"><?= $kir['content']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="usr">Link Read More :</label>
                                        <input type="text" class="form-control" id="link-kiri" name="link-kiri" value="<?= $kir['link']; ?>">
                                    </div>

                                <?php endforeach; ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

            </div>
            <!-- #/paper bg -->
        </div>
        <!-- ./wrap-sidebar-content -->

        <!-- / END OF CONTENT -->