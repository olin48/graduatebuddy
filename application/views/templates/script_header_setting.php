<script>
    (function($) {
        var dataMenuSetting = $('#dataMenuSetting').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_menu') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 3, 4],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });

        var dataSubMenuSetting = $('#dataSubMenuSetting').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_submenu') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 4, 5],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_menu_setting(id) {
        $('#form_edit_menu').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_menu_setting') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#menu_id_edit').val(data.id);
                $('#menu_name_edit').val(data.menu_name);
                $('#menu_link_edit').val(data.link);
                $('select[name="status_login_edit"]').val(data.flag);
                $('select[name="menu_status_id"]').val(data.is_active);
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                }
                $('#view_status').append(status);
                $('#edit-menu-setting').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_menu_setting(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_menu_setting/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataMenuSetting').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }

    function edit_submenu_setting(id) {
        $('#form_edit_submenu').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_submenu_setting') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#submenu_id_edit').val(data.id);
                $('select[name="submenu_menu_id_edit"]').val(data.menu_id);
                $('#submenu_name_edit').val(data.name_sub);
                $('#submenu_link_edit').val(data.link);
                $('select[name="submenu_status_id_edit"]').val(data.is_active);
                $('select[name="sub_status_login_edit"]').val(data.flag);
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                }
                $('#view_status').append(status);
                $('#edit-submenu-setting').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Submenu'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_submenu_setting(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_submenu_setting/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataSubMenuSetting').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>