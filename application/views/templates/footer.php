<!-- FOOTER -->
<div id="footer">
    <div class="devider-footer-left"></div>
    <div class="time">
        <p id="spanDate"></p>
        <p id="clock"></p>
    </div>
    <?php foreach ($web as $w) : ?>
        <div class="copyright">Copyright © <?php echo getdate()['year']; ?> <a href="<?= $w['link_copyright']; ?>" target="_blank"><?= $w['copyright']; ?></a>. All rights reserved.</div>
    <?php endforeach ?>
    <div class="devider-footer"></div>
    <ul>
        <li><i class="fa fa-facebook-square"></i>
        </li>
        <li><i class="fa fa-twitter-square"></i>
        </li>
        <li><i class="fa fa-instagram"></i>
        </li>
    </ul>
</div>
<!-- / FOOTER -->
</div>
<!-- Container -->


<!-- 
    ================================================== -->
<!-- Main jQuery Plugins -->
<script type='text/javascript' src="<?= base_url('assets/js/jquery-2.2.3.min.js'); ?>"></script>

<script type='text/javascript' src='<?= base_url('assets/js/bootstrap.js'); ?>'></script>
<script type='text/javascript' src='<?= base_url('assets/js/date.js'); ?>'></script>
<script type='text/javascript' src='<?= base_url('assets/js/slimscroll/jquery.slimscroll.js'); ?>'></script>
<script type='text/javascript' src='<?= base_url('assets/js/jquery.nicescroll.min.js'); ?>'></script>
<script type='text/javascript' src='<?= base_url('assets/js/sliding-menu.js'); ?>'></script>
<script type='text/javascript' src='<?= base_url('assets/js/scriptbreaker-multiple-accordion-1.js'); ?>'></script>
<script type='text/javascript' src="<?= base_url('assets/js/donut-chart/jquery.drawDoughnutChart.js'); ?>"></script>
<script type='text/javascript' src="<?= base_url('assets/js/tab/jquery.newsTicker.js'); ?>"></script>
<script type='text/javascript' src="<?= base_url('assets/js/tab/app.ticker.js'); ?>"></script>
<script type='text/javascript' src='<?= base_url('assets/js/app.js'); ?>'></script>

<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/js/datatables/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>

<script type='text/javascript' src='<?= base_url('assets/js/vegas.js'); ?>'></script>

<script type='text/javascript' src="<?= base_url('assets/js/number-progress-bar/jquery.velocity.min.js'); ?>'); ?>"></script>
<script type='text/javascript' src="<?= base_url('assets/js/number-progress-bar/number-pb.js'); ?>"></script>
<script src="<?= base_url('assets/js/loader/loader.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/loader/demo.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('assets/js/skycons/skycons.js'); ?>"></script>

<script src="<?= base_url('assets/js/textEditor/dist/wysihtml5-0.4.0pre.js'); ?>"></script>
<script src="<?= base_url('assets/js/textEditor/lib/js/wysihtml5-0.3.0.js'); ?>"></script>
<script src="<?= base_url('assets/js/textEditor/src/bootstrap3-wysihtml5.js'); ?>"></script>

<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/jquery.tagsinput-revisited.js'); ?>"></script>

<script type="text/javascript" src="<?= base_url('assets/js/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/datepicker/clockface.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/datepicker/bootstrap-datetimepicker.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

<!-- TAB SLIDER -->

<script src="<?= base_url('assets/js/chartist/chartist.min.js'); ?>"></script>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

<script>
    function logout() {
        window.location.href = "<?= base_url('cms/logout'); ?>";
    }

    (function($) {
        $('#dataMenu').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
        $('#dataSubmenu').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    })(jQuery);

    //Animation Slider
    $(function() {
        function randomPercentage() {
            return Math.floor(Math.random() * 100);
        }

        function randomInterval() {
            var min = Math.floor(Math.random() * 30);
            var max = min + (Math.floor(Math.random() * 40) + 70);
            return [min, max];
        }

        function randomStep() {
            return Math.floor(Math.random() * 10) + 5;
        }

        // setup
        var $basic = $('#basic');
        var interval = randomInterval();
        var basicBar = $basic.find('.number-pb').NumberProgressBar({
            style: 'basic',
            min: interval[0],
            max: interval[1]
        })
        $basic.find('.title span').text('[Min: ' + interval[0] + ', Max: ' + interval[1] + ']');

        var percentageBar = $('#percentage .number-pb').NumberProgressBar({
            style: 'percentage'
        })

        var $step = $('#step');
        var maxStep = randomStep()
        var stepBar = $('#step .number-pb').NumberProgressBar({
            style: 'step',
            max: maxStep
        })
        $step.find('.title span').text('[Max step: ' + maxStep + ']');

        // loop
        var basicLoop = function() {
            basicBar.reach(undefined, {
                complete: percentageLoop
            });
        }

        var percentageLoop = function() {
            percentageBar.reach(undefined, {
                complete: stepLoop
            });
        }

        var stepLoop = function() {
            stepBar.reach(undefined, {
                complete: basicLoop
            });
        }

        // start
        basicLoop();
    });
</script>