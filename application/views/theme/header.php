<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <?php foreach ($logo as $l) : ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?= $l['judul']; ?></title>
        <link rel="icon" href="<?= base_url('assets/front/img/favicon.png'); ?>">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/bootstrap.min.css'); ?>">
        <!-- animate CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/animate.css'); ?>">
        <!-- owl carousel CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/owl.carousel.min.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/front/css/owl.theme.default.min.css'); ?>">
        <!-- themify CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/themify-icons.css'); ?>">
        <!-- flaticon CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/flaticon.css'); ?>">
        <!-- font awesome CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/magnific-popup.css'); ?>">
        <!-- swiper CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/slicknav.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/front/css/slick.css'); ?>">
        <!-- style CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/style.css'); ?>">
        <!-- notify CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/front/css/jnoty.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-select.min.css'); ?>">
        <!-- fontawesome -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            html {
                scroll-behavior: smooth;
            }

            @media only screen and (min-width: 480px) {
                .element-popup {
                    width: 100%;
                    display: table;
                    margin: 10px 0px;
                    padding: 10px;
                    box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                }

                .text-popup {
                    display: table-cell;
                    height: 100%;
                    width: 50%;
                    vertical-align: middle;
                    background: white;
                }

                .img-popup {
                    display: table-cell;
                    width: 90%;
                    height: auto;
                }
            }

            @media only screen and (max-width: 479px) {
                .element-popup {
                    width: 100%;
                    margin: 10px 0px;
                    padding: 10px;
                    box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                }

                .text-popup {
                    background: white;
                }

                .img-popup {
                    width: 50%;
                    margin: 0px auto;
                    display: block;
                    height: auto;
                }
            }
        </style>
    <?php endforeach; ?>
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu <?php echo $page; ?>">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <?php foreach ($logo as $l) : ?>
                            <a class="navbar-brand" href="<?= $l['content']; ?>"> <img src="<?= base_url('assets/uploads/logo/'); ?><?= $l['image']; ?>" alt="<?= $l['judul']; ?>" height="60px"> </a>
                        <?php endforeach; ?>
                        <button class="navbar-toggler" type="button" style="background-color: #f44a40;" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <?php foreach ($menu_header as $mh) : ?>
                                    <li class="nav-item <?php if ($mh['link'] == "#") {
                                                            echo 'dropdown';
                                                        } ?>">
                                        <a class="nav-link <?php if ($mh['link'] == "#") {
                                                                echo 'dropdown-toggle';
                                                            } ?>" href="<?= base_url(); ?><?= $mh['link']; ?>"><?= $mh['menu_name']; ?></a>

                                        <?php if ($mh['link'] == "#") {
                                            echo "<div class='dropdown-menu' aria-labelledby='navbarDropdown'>";
                                        } ?>
                                        <?php
                                        $menuId = $mh['id'];
                                        if ($is_login == null) {
                                            $querySubMenu = "SELECT cms_submenu_settings.link, 
                                                cms_submenu_settings.name_sub FROM `cms_submenu_settings`
                                                JOIN `cms_menu_settings` 
                                                ON `cms_submenu_settings`.`menu_id` = `cms_menu_settings`.`id`
                                                WHERE `cms_submenu_settings`.`menu_id` = $menuId
                                                AND `cms_submenu_settings`.`is_active` = '1'
                                                AND `cms_submenu_settings`.`flag` = 'not_login'";
                                        } else {
                                            $querySubMenu = "SELECT cms_submenu_settings.link, 
                                                cms_submenu_settings.name_sub FROM `cms_submenu_settings`
                                                JOIN `cms_menu_settings` 
                                                ON `cms_submenu_settings`.`menu_id` = `cms_menu_settings`.`id`
                                                WHERE `cms_submenu_settings`.`menu_id` = $menuId
                                                AND `cms_submenu_settings`.`is_active` = '1'
                                                AND `cms_submenu_settings`.`flag` = 'login'";
                                        }

                                        $subMenu = $this->db->query($querySubMenu)->result_array();
                                        ?>
                                        <?php foreach ($subMenu as $sm) : ?>
                                            <a class="dropdown-item" href="<?= base_url(); ?><?= $sm['link']; ?>"><?= $sm['name_sub']; ?></a>
                                        <?php endforeach; ?>
                                        <?php if ($mh['link'] == "#") {
                                            echo "</div>";
                                        } ?>
                                    </li>
                                <?php endforeach; ?>

                                <li class="nav-item">
                                    <?php if ($is_login == null) {
                                        echo "<button class='btn post-btn py-3 px-4' data-toggle='modal' data-target='#login'>Login</button>";
                                    } else {
                                        echo "<button class='btn post-btn py-3 px-4' onclick='logout()'>Logout</button>";
                                    }
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <?= $this->session->flashdata('message'); ?>
    </header>
    <!-- Header part end-->

    <!-- Modal -->
    <div id="register" class="modal fade" role="dialog">
        <div class="modal-dialog" style="margin: absolut; top: 15%; padding: 10px;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Beli Paket Sekarang</h2>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="frontend/register_member" method="post" oninput='password2.setCustomValidity(password2.value != password1.value ? "Passwords do not match." : "")'>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Nama Lengkap :</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Email :</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">No Handphone Aktif :</label>
                            <input type="tel" pattern=".{10,}" class="form-control" id="phone" name="phone" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Password :</label>
                            <input type="password" class="form-control" id="password1" name="password1" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Ketik Ulang Password :</label>
                            <input type="password" class="form-control" id="password2" name="password2" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Pilihan Paket :</label>
                            <select id="paket" class="form-control" required>
                                <?php foreach ($paket_pilih as $pp) : ?>
                                    <option value="<?= $pp['id']; ?>"><?= $pp['nama_paket']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <!-- PHP CODING BUAT AMBIL LIST PAKET -->
                        </div>
                        <div class="modal-footer">
                            <h6 style="position: absolute; left:0px; padding-left:15px;">Bingung ? <a href="whatsapp:contact=081806122995@s.whatsapp.com&message=' I&#39;d like to chat with you" style="color: #f44a40;">Hubungi Admin</a></h6>
                            <button type=" submit" class="btn hero-btn">OK, Saya Beli</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="login" class="modal fade" role="dialog">
        <div class="modal-dialog" style="margin: absolut; top: 15%; padding: 10px;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Login ke Akun Buddy Kamu</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form role="form" method="post" action="<?= base_url('frontend/auth'); ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">No Handphone :</label>
                            <input type="text" class="form-control" id="phone" name="phone" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Password :</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn post-btn">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                        <br>
                    </div>
                </form>
                <h6 style="text-align:center;"><a data-target="#resetpassword" data-toggle="modal" class="MainNavText" id="MainNavHelp" data-dismiss="modal" href="#resetpassword">Lupa Password</a>
                </h6>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>

    <div id="resetpassword" class="modal fade" role="dialog">
        <div class="modal-dialog" style="margin: absolut; top: 15%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Lupa Password</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div>
                    <br />
                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;Silahkan masukkan Email terdaftar kamu</h6>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" action="<?= base_url('frontend/forgot_password'); ?>">
                        <div class="form-group">
                            <label for="usr">Email :</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn post-btn" style="align: left" onclick="openConfirm()">Reset Password Saya</button><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="confirm" class="modal fade" role="dialog">
        <div class="modal-dialog" style="margin: absolut; top: 15%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="element-popup">
                        <img src="<?= base_url(); ?>assets/img/staff-7.jpg" class="img-popup">
                        <div class="text-popup">
                            Password akun Buddy kamu sudah diproses. Silahkan cek <b>inbox email </b>kamu untuk melihat password kamu. <br><br>
                            Jika ada pertanyaan, silahkan hubungin Admin dengan menekan tombol berikut : <br><br>
                            <h5><a href="whatsapp:contact=081806122995@s.whatsapp.com&message=' I&#39;d like to chat with you" style="color: #f44a40;">Hubungi Admin</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>