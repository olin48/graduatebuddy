<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <!-- <h2>Service</h2> 
						<p style="align: left"><a href="<?= base_url('home'); ?>" style="color: #f44a40">Home</a><span>/</span>Service</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!--::review_part start::-->
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="section_tittle text-center">
                <p>Our Services</p>
                <h2>Buddy you can rely on</h2>
            </div>
        </div>
        <div class="row tabulation mt-4 ftco-animate">
            <div class="col-md-4">
                <ul class="nav nav-pills nav-fill d-md-flex d-block flex-column">
                    <li class="nav-item text-left">
                        <a class="nav-link active py-3" data-toggle="tab" href="#services-1">CV Clinic and Improvement</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-3" data-toggle="tab" href="#services-2">Interview Preparation</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-3" data-toggle="tab" href="#services-3">Psychotest Drilling</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-3" data-toggle="tab" href="#services-4">Career Consultation</a>
                    </li>
                    <!-- <li class="nav-item text-left">
                        <a class="nav-link py-3" data-toggle="tab" href="#services-5">Personal Problem</a>
                    </li>
                    <li class="nav-item text-left">
                        <a class="nav-link py-3" data-toggle="tab" href="#services-6">Business Problem</a>
                    </li> -->
                </ul>
            </div>
            <div class="col-md-8">
                <div class="tab-content">
                    <div class="tab-pane container p-0 active" id="services-1">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_1.jpg'); ?>);"></div>
                        <h4><a href="#">CV Clinic and Improvement</a></h4>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-2">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_2.jpg'); ?>);"></div>
                        <h4><a href="#">Interview Preparation</a></h4>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-3">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_3.jpg'); ?>);"></div>
                        <h4><a href="#">Psychotest Drilling</a></h4>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-4">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_4.jpg'); ?>);"></div>
                        <h4><a href="#">Career Consultation</a></h4>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <!-- <div class="tab-pane container p-0 fade" id="services-5">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image_5.jpg'); ?>);"></div>
                        <h3><a href="#">Personal Problem</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div>
                    <div class="tab-pane container p-0 fade" id="services-6">
                        <div class="img" style="background-image: url(<?= base_url('assets/img/image-6.jpg'); ?>);"></div>
                        <h3><a href="#">Business Problem</a></h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="feature_part">
    <div class="container">
        <div class="row justify-content-center" style="padding-top: 50px;">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>Pilihan Paket</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" style="padding-bottom: 50px;">
            <?php foreach ($paket_pilih as $pp) : ?>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon">
                                <i class="<?= $pp['icon']; ?> style='vertical-align: middle'"></i>
                            </span>
                            <h4><?= $pp['nama_paket']; ?></h4>
                            <p><?= $pp['keterangan']; ?></p>
                            <br />
                            <p>Rp. <?= number_format($pp['harga_paket'], 2, ',', '.'); ?></p>
                            <br />
                            <a href="#" data-toggle='modal' data-target='#register' class="btn_1">Pilih Paket</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>