<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <!-- <h2>Free CV Template</h2>
                        <p><a href="<?= base_url('home'); ?>" style="color: #f44a40">Home</a><span>/</span>Free CV Template</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="section_tittle text-center">
                <h2>Find Top Talent In Half the Time</h2>
            </div><br>
            <div class="text-center" style="margin-top: 50px;">
                <p>From thousands of candidate signups on Glints, we curate the top 3% for you. Whether it's for urgent or hard-to-fill roles, TalentHunt can got you a candidate within 2-4 weeks.</p>
            </div>
            <div class="text-center" style="margin-top: 50px;">
                <h2>Benefit</h2>
            </div>
            <div class="row">
                <div class="col-md-2 d-flex align-items-stretch ftco-animate" style="margin-right: 50px;">
                    <div class="services-2 text-center">
                        <div class="icon-wrap">
                            <div class="number d-flex align-items-center justify-content-center"><span>01</span></div>
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="fa fa-smile-o"></span>
                            </div>
                        </div>
                        <p>Tema template yang kekinian dan unik</p>
                    </div>
                </div>
                <div class="col-md-2 d-flex align-items-stretch ftco-animate" style="margin-right: 50px;">
                    <div class="services-2 text-center">
                        <div class="icon-wrap">
                            <div class="number d-flex align-items-center justify-content-center"><span>02</span></div>
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="fa fa-file-text-o"></span>
                            </div>
                        </div>
                        <p>Format template sudah terdiri dari info-info yang dianjurkan tercantum</p>
                    </div>
                </div>
                <div class="col-md-2 d-flex align-items-stretch ftco-animate" style="margin-right: 50px;">
                    <div class="services-2 text-center">
                        <div class="icon-wrap">
                            <div class="number d-flex align-items-center justify-content-center"><span>03</span></div>
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="fa fa-clipboard"></span>
                            </div>
                        </div>
                        <p>Bisa digunakan untuk melamar di industri apapun</p>
                    </div>
                </div>
                <div class="col-md-2 d-flex align-items-stretch ftco-animate" style="margin-right: 50px;">
                    <div class="services-2 text-center">
                        <div class="icon-wrap">
                            <div class="number d-flex align-items-center justify-content-center"><span>04</span></div>
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="fa fa-pencil"></span>
                            </div>
                        </div>
                        <p>Bisa diedit dan dikreasikan sesuai kebutuhan kamu</p>
                    </div>
                </div>
                <div class="col-md-2 d-flex align-items-stretch ftco-animate">
                    <div class="services-2 text-center">
                        <div class="icon-wrap">
                            <div class="number d-flex align-items-center justify-content-center"><span>05</span></div>
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="fa fa-money"></span>
                            </div>
                        </div>
                        <h2>GRATIS!</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--? Services Area Start-->
<div class="support-company-area pt-100 pb-100 section-bg fix" data-background="<?= base_url('assets/front/img/gallery/section_bg05.png'); ?>" style="background-image: url('<?= base_url('assets/front/img/gallery/section_bg05.png'); ?>');">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-5 col-lg-5" style="background-color: aliceblue;">
                <form action="frontend/download_cv_template" method="post">
                    <?= $this->session->flashdata('message_download_cv_template'); ?>
                    <div class="form-group" style="margin-top: 30px;">
                        <label for="usr"><b>Nama :</b></label>
                        <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Nama Lengkap" required>
                    </div>
                    <div class="form-group">
                        <label for="usr"><b>Email :</b></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="usr"><b>No. HP :</b></label>
                        <input type="tel" pattern=".{10,}" class="form-control" id="no_hp" name="no_hp" placeholder="No HP" required>
                    </div>
                    <div class="form-group">
                        <label for="usr"><b>Umur :</b></label>
                        <input type="text" class="form-control" id="umur" name="umur" placeholder="Usia Anda Saat Ini">
                    </div>
                    <div class="form-group">
                        <label for="usr"><b>Universitas :</b></label>
                        <input type="text" class="form-control" id="universitas" name="universitas" placeholder="Nama Universitas">
                    </div>
                    <!-- <div class="form-group">
                        <label for="usr"><b>Bidang Minat :</b></label>
                        <input type="text" class="form-control" id="bidang_minat" name="bidang_minat" placeholder="Bidang minat">
                    </div> -->
                    <button type="submit" class="btn post-btn" style="margin-bottom: 30px;">Download CV Template</button>
                </form>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="right-caption">
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Services Area End-->