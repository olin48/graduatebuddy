<main>
	<!-- banner part start-->
	<div class="slider-area" id="backtotop">
		<div class="slider-active">
			<!-- Single Slider -->
			<div class="single-slider slider-height d-flex align-items-center slider-area" style="background-image: url(<?= base_url('assets/front/img/gallery/h1_hero.jpg'); ?>); width:100%">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-lg-7 col-md-8">
							<div class="hero__caption">
								<!-- <span data-animation="fadeInLeft" data-delay=".1s">Committed to success</span> -->
								<h1 data-animation="fadeInLeft" data-delay=".5s">A Buddy you can rely on for your future</h1>
								<p data-animation="fadeInLeft" data-delay=".9s">1-on-1 coaching persiapan seleksi kerja</p>
								<!-- Hero-btn -->
								<div class="hero__btn" data-animation="fadeInLeft" data-delay="1.1s">
									<a href="service" class="btn hero-btn">Daftar Sekarang</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="single-slider slider-height d-flex align-items-center slider-area" style="background-image: url(<?= base_url('assets/front/img/single_project.png'); ?>); width:100%">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-lg-7 col-md-8">
							<div class="hero__caption">
								<!-- <span data-animation="fadeInLeft" data-delay=".1s">Committed to success</span> -->
								<h1 data-animation="fadeInLeft" data-delay=".5s">XXXXX</h1>
								<p data-animation="fadeInLeft" data-delay=".9s">1-on-1 coaching persiapan seleksi kerja</p>
								<!-- Hero-btn -->
								<div class="hero__btn" data-animation="fadeInLeft" data-delay="1.1s">
									<a href="service" class="btn hero-btn">Daftar Sekarang</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- banner part start-->

	<!--? About Area Start-->
	<div id="about" class="support-company-area pt-10 pb-10 section-bg fix" data-background="<?= base_url('assets/consultingbiz/img/gallery/section_bg05.jpg'); ?>" style="background-image: url('<?= base_url('assets/consultingbiz/img/gallery/section_bg05.jpg'); ?>');">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-xl-6 col-lg-6">
					<div class="left-caption">
						<!-- Section Tittle -->
						<div class="section-tittle section-tittle2 mb-50">
							<!-- <span>Our Top Services</span> -->
							<h3 style="color: black">Perkenalkan, kami </h3>
							<h2 style="color: navy">GraduateBuddy</h2>
						</div>
						<div class="support-caption">
							<p class="pera-top">Graduate buddy merupakan program coaching 1-on-1 yang dirancang khusus dalam membantu para fresh graduate mendapatkan pekerjaan impian. Kami menghubungkan lulusan baru dengan para professional handal yang sudah berpengalaman dibidang tes dan seleksi kerja.</p>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6">
					<div class="support-location-img">
						<img src="<?= base_url('assets/consultingbiz/img/gallery/home_blog1.png'); ?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- About Area End-->

	<!--? Services Area Start-->
	<div class="support-company-area pt-10 pb-10 section-bg fix" data-background="<?= base_url('assets/front/img/gallery/section_bg02.jpg'); ?>" style="background-image: url('<?= base_url('assets/consultingbiz/img/gallery/section_bg02.jpg'); ?>');">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-xl-6 col-lg-6">
					<div class="support-location-img">
						<img src="<?= base_url('assets/consultingbiz/img/gallery/about.png'); ?>" alt="">
					</div>
				</div>
				<div class="col-xl-6 col-lg-6">
					<div class="right-caption">
						<!-- Section Tittle -->
						<div class="section-tittle section-tittle2 mb-50">
							<!-- <span>Our Top Services</span>-->
							<h3 style="color: white">Layanan kami </h3>
							<h2>Our Services</h2>
						</div>
						<div class="support-caption">
							<p class="pera-top">Buddy akan memberikan pelatihan dan sesi coaching persiapan tes seleksi masuk kerja yang disesuaikan dengan kebutuhan masing-masing client, sehingga diharpakan client dapat memiliki keterampilan/skills yang dibutuhkan untuk lolos dalam seleksi kerja</p>
							<p class="mb-65"></p>
							<a href="<?= base_url('service'); ?>" class="btn post-btn">Lihat Layanan</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Services Area End-->

	<!-- Brand Area Start -->
	<div class="brand-area pb-10 pt-50">
		<div class="container">
			<div class="col-md-20 text-center heading-section heading-section-white ftco-animate">
				<h4 style="color: black">Kami sudah berhasil membantu lebih dari 300 Fresh Graduate </h4>
				<h4 style="color: black">diterima bekerja di Perusahaan impian mereka :</h4>
			</div>
			<div class="brand-active brand-border pb-30 pt-30">
				<div class="single-brand">
					<img src="<?= base_url('assets/front/img/gallery/brand1.png'); ?>" alt="">
				</div>
				<div class="single-brand">
					<img src="<?= base_url('assets/front/img/gallery/brand2.png'); ?>" alt="">
				</div>
				<div class="single-brand">
					<img src="<?= base_url('assets/front/img/gallery/brand3.png'); ?>" alt="">
				</div>
				<div class="single-brand">
					<img src="<?= base_url('assets/front/img/gallery/brand4.png'); ?>" alt="">
				</div>
				<div class="single-brand">
					<img src="<?= base_url('assets/front/img/gallery/brand2.png'); ?>" alt="">
				</div>
				<div class="single-brand">
					<img src="<?= base_url('assets/front/img/gallery/brand5.png'); ?>" alt="">
				</div>
			</div>
		</div>
	</div>
	<!-- Brand Area End -->

	<!--? Services Ara Start -->
	<div class="services-area section-padding1" data-background="<?= base_url('assets/front/img/gallery/section_bg02.jpg'); ?>" style="background-image: url('<?= base_url('assets/consultingbiz/img/gallery/section_bg04.jpg'); ?>');">
		<div class="container">
			<div class="row">
				<div class="cl-xl-7 col-lg-12 col-md-12">
					<!-- Section Tittle -->
					<div class="section-tittle mb-30 mt-30 text-center ">
						<!-- <span>Our Portfolios of cases</span>-->
						<h3 style="color: white">Testimonial</h3>
						<h2 style="color: white">Panduan E-Book</h2>
					</div>
					<!--<div class="col-md-15 pt-30 pb-30 text-center heading-section heading-section-white ftco-animate">
					<span class="subheading">Testimonial</span>
					<h2 class="mb-3">Panduan E-Book</h2>
				</div>-->
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-10">
					<div class="single-services mb-20">
						<div class="services-img">
							<a href="service"><img src="<?= base_url('assets/front/img/gallery/services1.png'); ?>" alt=""></a>
						</div>
						<div class="services-caption">
							<span>CV Clinic and Improvement</span>
							<p><a href="#">Improve CV kamu jadi lebih bernilai</a></p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-10">
					<div class="single-services mb-50">
						<div class="services-img">
							<a href="service"><img src="<?= base_url('assets/front/img/gallery/services2.png'); ?>" alt=""></a>
						</div>
						<div class="services-caption">
							<span>Interview Preparation</span>
							<p><a href="#">Interview Preparation</a></p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-10">
					<div class="single-services mb-100">
						<div class="services-img">
							<a href="service"><img src="<?= base_url('assets/front/img/gallery/services3.png'); ?>" alt=""></a>
						</div>
						<div class="services-caption">
							<span>Psychotest Drilling</span>
							<p><a href="#">Psychotest Drilling</a></p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-10">
					<div class="single-services mb-100">
						<div class="services-img">
							<a href="service"><img src="<?= base_url('assets/front/img/gallery/services4.png'); ?>" alt=""></a>
						</div>
						<div class="services-caption">
							<span>Career Consultation</span>
							<p><a href="#">Career Consultation</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Services Ara End -->

	<!-- Testimoni Start -->
	<section class="ftco-section testimony-section">
		<div class="img img-bg border" style="background-image: url(<?= base_url('assets/front/img/gallery/bg_4.jpg'); ?>);"></div>
		<div class="overlay"></div>
		<div class="container">
			<!--<div class="row justify-content-center mb-5">
			<div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
				<span class="subheading">Testimonial</span>
				<h2 class="mb-3">Happy Clients</h2>
			</div>-->
			<div class="cl-xl-7 col-lg-12 col-md-12">
				<div class="section-tittle mb-30 mt-30 text-center ">
					<!-- <span>Our Portfolios of cases</span>-->
					<h3 style="color: white">Testimonial</h3>
					<h2 style="color: white">Happy Clients</h2>
				</div>
			</div>
			<div class="row ftco-animate">
				<div class="col-md-12">
					<div class="carousel-testimony owl-carousel ftco-owl">
						<?php foreach ($testimoni as $testi) : ?>
							<div class="item">
								<div class="testimony-wrap py-4 px-6">
									<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
									<div class="text">
										<p class="mb-4"><?= $testi['komentar']; ?></p>
										<div class="d-flex align-items-center">
											<div class="user-img" style="background-image: url('<?php echo base_url(); ?>/assets/uploads/single/<?= $testi['photo']; ?>')"></div>
											<div class="pl-3">
												<p class="name"><?= $testi['nama']; ?></p>
												<span class="position"><?= $testi['profesi']; ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimoni End -->

	<!-- Want To work -->
	<section class="wantToWork-area w-padding2 section-bg" data-background="<?= base_url('assets/consultingbiz/img/gallery/section_bg03.jpg'); ?>" style="background-image: url('<?= base_url('assets/consultingbiz/img/gallery/section_bg03.jpg'); ?>');">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-xl-7 col-lg-9 col-md-8">
					<div class="wantToWork-caption wantToWork-caption2">
						<h2>Masa Depan Cerah<br> Di Depan Mata</h2>
					</div>
				</div>
				<div class="col-xl-4 col-lg-3 col-md-2">
					<a href="service" class="btn btn-black f-right">Bergabung Sekarang</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Want To work End -->
	<!-- Blog Start -->
	<section class="ftco-section" style="margin-top: 10px;">
		<div class="container">
			<div class="cl-xl-7 col-lg-12 col-md-12">
				<div class="section-tittle mb-30 mt-30 text-center ">
					<!-- <span>Our Portfolios of cases</span>-->
					<h3>Blog</h3>
					<h2>Recent Blogs</h2>
				</div>
			</div>
			<div class="row d-flex">
				<?php foreach ($blog as $b) : ?>
					<div class="col-md-4 d-flex ftco-animate">
						<div class="blog-entry justify-content-end">
							<div class="text text-center">
								<a href="<?= base_url('blog/post/'); ?><?= $b['post_slug']; ?>" class="block-20 img" style="background-image: url('<?php echo base_url(); ?>/assets/uploads/single/<?= $b['image']; ?>');">
								</a>
								<div class="meta text-center mb-2 d-flex align-items-center justify-content-center">
									<div>
										<span class="day"><?= date('d', strtotime($b['created_date'])); ?></span>
										<span class="mos"><?= date('M', strtotime($b['created_date'])); ?></span>
										<span class="yr"><?= date('Y', strtotime($b['created_date'])); ?></span>
									</div>
								</div>
								<h3 class="heading mb-3"><a href="<?= base_url('blog/post/'); ?><?= $b['post_slug']; ?>"><?= word_limiter($b['title'], 5); ?></a></h3>
								<p><?= word_limiter($b['post_text'], 10); ?></p>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>

	<div id="popup_promosi" class="modal fade" role="dialog">
		<div class="modal-dialog" style="margin: absolut; top: 15%;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="element-popup">
						<img src="<?= base_url(); ?>assets/img/staff-7.jpg" class="img-popup">
						<div class="text-popup">
							ONLY TODAY!
							<h1>30% OFF</h1>
							The key victory is discipline and that means a well made bed.<br /><br />
							<a href="<?= base_url('service'); ?>" class="btn_1 py-2 px-3">Daftar Sekarang!</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Blog End -->