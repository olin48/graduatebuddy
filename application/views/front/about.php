<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>About Us</h2>
                        <p><a href="<?= base_url('home'); ?>" style="color: #f44a40">Home</a><span>/</span>About Us</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!-- learning part start-->
<?php foreach ($about_atas as $abat) : ?>
    <div id="about" class="support-company-area pt-100 pb-100 section-bg fix" <?php if ($abat['is_active'] != 1) {
                                                                                    echo "hidden";
                                                                                } ?>>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <?= $abat['content']; ?>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="support-location-img">
                        <img src="<?php echo base_url('/assets/uploads/single/'); ?><?= $abat['image']; ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<!-- learning part end-->

<!-- learning part start-->
<?php foreach ($about_bawah as $abaw) : ?>
    <section class="advance_feature learning_part" <?php if ($abaw['is_active'] != 1) {
                                                        echo "hidden";
                                                    } ?>>
        <div class="container">
            <div class="row align-items-sm-center align-items-xl-stretch">
                <div class="col-md-6 col-lg-6">
                    <div class="learning_member_text">
                        <?= $abaw['content']; ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="learning_img">
                        <img src="<?php echo base_url('/assets/uploads/single/'); ?><?= $abaw['image']; ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endforeach; ?>
<!-- learning part end-->