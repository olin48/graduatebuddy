<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg" style="background-image: url('<?= base_url('assets/uploads/single/'); ?><?= $post_row['image']; ?>')">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->
<!--================Blog Area =================-->
<section class="blog_area single-post-area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-list">
                <div class="single-post">
                    <div class="feature-img">
                        <img class="img-fluid" src="<?= base_url('assets/uploads/single/'); ?><?= $post_row['image']; ?>" alt="">
                    </div>
                    <div class="blog_details">
                        <h2><?= $post_row['title']; ?></h2>
                        <ul class="blog-info-link mt-3 mb-4">
                            <li><a href="#"><i class="fa fa-tag"></i><?= $post_row['post_tag']; ?></a></li>
                            <li><a href="#"><i class="fa fa-calendar"></i><?= date('d F Y', strtotime($post_row['created_date'])); ?></a></li>
                            <li><a href="#"><i class="fa fa-comments"></i> <?= $count_comment; ?> Komentar</a></li>
                        </ul>
                        <?= $post_row['post_text']; ?>
                    </div>
                </div>
                <div class="navigation-top">
                    <div class="d-sm-flex justify-content-between text-center">
                        <div class="col-sm-4 text-center my-2 my-sm-0">
                            <!-- <p class="comment-count"><span class="align-middle"><i class="far fa-comment"></i></span> 06 Comments</p> -->
                        </div>
                        <!-- <ul class="social-icons">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fab fa-behance"></i></a></li>
                        </ul> -->
                    </div>
                    <!-- <div class="navigation-area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                <div class="thumb">
                                    <a href="#">
                                        <img class="img-fluid" src="img/post/preview.png" alt="">
                                    </a>
                                </div>
                                <div class="arrow">
                                    <a href="#">
                                        <span class="lnr text-white ti-arrow-left"></span>
                                    </a>
                                </div>
                                <div class="detials">
                                    <p>Prev Post</p>
                                    <a href="#">
                                        <h4>Space The Final Frontier</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                                <div class="detials">
                                    <p>Next Post</p>
                                    <a href="#">
                                        <h4>Telescopes 101</h4>
                                    </a>
                                </div>
                                <div class="arrow">
                                    <a href="#">
                                        <span class="lnr text-white ti-arrow-right"></span>
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#">
                                        <img class="img-fluid" src="img/post/next.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="blog-author">
                    <div class="media align-items-center">
                        <img src="<?= base_url('assets/uploads/profile/'); ?><?= $post_row['profile']; ?>" alt="">
                        <div class="media-body">
                            <a href="#">
                                <h4><?= $post_row['first_name']; ?> <?= $post_row['last_name']; ?></h4>
                            </a>
                            <p><?= $post_row['about_me']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="comments-area">
                    <h4><?= $count_comment; ?> Komentar</h4>
                    <?php foreach ($get_comment as $gc) : ?>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="<?= base_url('assets/uploads/profile/'); ?><?= $gc['image']; ?>" alt="">
                                    </div>
                                    <div class="desc">
                                        <p class="comment"><?= $gc['comment_post']; ?></p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5>
                                                    <a href="<?= $gc['website']; ?>" target="_blank"><?= $gc['first_name']; ?> <?= $gc['last_name']; ?></a>
                                                </h5>
                                                <p class="date"><?= date('d F Y', strtotime($gc['created_date'])); ?> at <?php echo date('H:i:s', strtotime($gc['created_date'])); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="comment-form">
                    <h4>Tinggalkan Komentar</h4>
                    <form action="<?= base_url('berita/post_comment/'); ?><?= $post_row['id']; ?>" method="post">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="slug_comment" id="slug_comment" type="hidden" placeholder="Id" value="<?= $post_row['post_slug']; ?>" required>
                                    <textarea class="form-control w-100" name="comment_text" id="comment_text" cols="30" rows="9" placeholder="Tulis Komentar Anda." required></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" name="name_comment" id="name_comment" type="text" placeholder="Name" value="<?= $name_user; ?>" required>
                                </div>
                            </div>
                            <div class=" col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" name="email_comment" id="email_comment" type="text" placeholder="Email" value="<?= $email_user; ?>" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" name="website_comment" id="website_comment" type="text" placeholder="https://graduatebuddy.id" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="button btn_1 button-contactForm">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>